<?php

/**
 * This file is part of etten/doctrine.
 * Copyright © 2017 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Doctrine;

use Doctrine\ORM;

class QueryFactory
{

    /**
     * @var ORM\EntityManager
     */
    private $em;

    public function __construct(ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function createQueryBuilder(): ORM\QueryBuilder
    {
        return $this->em->createQueryBuilder();
    }

}
