<?php

/**
 * This file is part of etten/doctrine.
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Doctrine;

use Doctrine\ORM\EntityManager;

class Persister
{

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * Flushes all changes to objects that have been queued up to now to the database.
	 * This effectively synchronizes the in-memory state of managed objects with the
	 * database.
	 *
	 * If an entity is explicitly passed to this method only this entity and
	 * the cascade-persist semantics + scheduled inserts/removals are synchronized.
	 * @param null|object|object[] $entity
	 * @return void
	 */
	public function flush($entity = NULL)
	{
		// EntityManager::flush() supports multiple values by default.
		$this->em->flush($entity);
	}

	/**
	 * Clears the EntityManager. All entities that are currently managed
	 * by this EntityManager become detached.
	 * @param null|string|string[] $entityName
	 * @return void
	 */
	public function clear($entityName = NULL)
	{
		$this->callRecursively($entityName, [$this->em, 'clear']);
	}

	/**
	 * Tells the EntityManager to make an instance managed and persistent.
	 *
	 * The entity will be entered into the database at or before transaction
	 * commit or as a result of the flush operation.
	 *
	 * NOTE: The persist operation always considers entities that are not yet known to
	 * this EntityManager as NEW. Do not pass detached entities to the persist operation.
	 * @param object|object[] $entity
	 * @return void
	 */
	public function persist($entity)
	{
		$this->callRecursively($entity, [$this->em, 'persist']);
	}

	/**
	 * Removes an entity instance.
	 *
	 * A removed entity will be removed from the database at or before transaction commit
	 * or as a result of the flush operation.
	 * @param object|object[] $entity
	 * @return void
	 */
	public function remove($entity)
	{
		$this->callRecursively($entity, [$this->em, 'remove']);
	}

	/**
	 * Refreshes the persistent state of an entity from the database,
	 * overriding any local changes that have not yet been persisted.
	 * @param object|object[] $entity
	 * @return void
	 */
	public function refresh($entity)
	{
		$this->callRecursively($entity, [$this->em, 'refresh']);
	}

	/**
	 * Detaches an entity from the EntityManager, causing a managed entity to
	 * become detached.  Unflushed changes made to the entity if any
	 * (including removal of the entity), will not be synchronized to the database.
	 * Entities which previously referenced the detached entity will continue to
	 * reference it.
	 * @param object|object[] $entity
	 * @return void
	 */
	public function detach($entity)
	{
		$this->callRecursively($entity, [$this->em, 'detach']);
	}

	/**
	 * Merges the state of a detached entity into the persistence context
	 * of this EntityManager and returns the managed copy of the entity.
	 * The entity passed to merge will not become associated/managed with this EntityManager.
	 * @param object|object[] $entity
	 * @return object|object[] The managed copy of the entity.
	 */
	public function merge($entity)
	{
		return $this->callRecursively($entity, [$this->em, 'merge']);
	}

	/**
	 * Determines whether an entity instance is managed in this EntityManager.
	 * @param object|array $entity
	 * @return bool|bool[]
	 */
	public function contains($entity)
	{
		return $this->callRecursively($entity, [$this->em, 'contains']);
	}

	private function callRecursively($input, callable $func)
	{
		// Support batch operations. Like original EntityManager::flush().
		if (is_array($input) || $input instanceof \Traversable) {
			$return = [];
			foreach ($input as $item) {
				$return = array_merge(
					$return,
					$this->callRecursively($item, $func)
				);
			}
		} else {
			$return = $func($input);
		}

		return $return;
	}

}
