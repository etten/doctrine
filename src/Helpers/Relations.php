<?php

/**
 * Copyright © 2017 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Doctrine\Helpers;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

class Relations
{

	/** @var EntityManager */
	private $em;

	/** @var array */
	private $orderBy = [NULL, NULL];

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	public function orderBy(string $column, string $order = 'asc')
	{
		$this->orderBy = [$column, $order];
	}

	public function fetchRelated(string $column, array $entities)
	{
		$entities = array_values($entities);

		$entity = current($entities);
		if ($entity) {
			$qb = $this->em->createQueryBuilder();

			$qb->select('PARTIAL e.{id}, data')
				->from(get_class($entity), 'e')
				->leftJoin(sprintf('e.%s', $column), 'data')
				->andWhere('e.id IN (:entities)')
				->setParameter('entities', $entities);

			$this->applyOrderBy($qb);

			$qb->getQuery()->getResult();
		}
	}

	private function applyOrderBy(QueryBuilder $qb)
	{
		list($column, $order) = $this->orderBy;

		if ($column) {
			$qb->orderBy(sprintf('e.%s', $column), $order);
		}
	}

}
