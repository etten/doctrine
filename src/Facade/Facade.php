<?php

/**
 * This file is part of etten/doctrine.
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Doctrine\Facade;

/**
 * @deprecated Use Etten\Doctrine\Facade instead.
 */
class Facade extends \Etten\Doctrine\Facade
{

}
