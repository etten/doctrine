<?php

/**
 * This file is part of etten/doctrine.
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Doctrine\Entities;

abstract class Entity implements IdProvider, HexIdProvider, Cacheable, \ArrayAccess, \IteratorAggregate
{

	use Attributes\Cache;
	use Attributes\MagicAccessors;

}
