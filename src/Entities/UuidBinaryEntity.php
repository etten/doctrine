<?php

/**
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Doctrine\Entities;

abstract class UuidBinaryEntity extends Entity
{

	use Attributes\UuidBinary;

}
